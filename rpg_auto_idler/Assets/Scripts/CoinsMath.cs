using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;

public class CoinsMath : MonoBehaviour
{
    static double coinsValue = 0;
    [SerializeField]
    double hero2Cost = 1;
    [SerializeField]
    double hero1Cost = 1;
    public TextMeshProUGUI coinsText;
    [SerializeField]
    double Hero1Multiplier = 0;
    [SerializeField]
    double Hero2Multiplier = 0;
    [SerializeField]
    double clickMultiplier = 1;
    double baseMultiplier = 0.1f;
    double heroCostMultiplier = 1.2f;
    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("Hero1CoinsMultiplier", 1f, 1f);
        InvokeRepeating("Hero2CoinsMultiplier", 1f, 1f);
    }

    // Update is called once per frame
    void Update()
    {
       
    }

    public void Hero1Increments()
    {
        if (hero1Cost <= coinsValue)
        {
            coinsValue -= hero1Cost;
            coinsValue = Math.Round(coinsValue, 2);
            coinsText.SetText(coinsValue.ToString());
            Hero1Multiplier += baseMultiplier;
            hero1Cost *= heroCostMultiplier;
        }
        else
            return;
    }

    public void Hero2Increments()
    {
        if (hero2Cost * 2 <= coinsValue)
        {
            coinsValue -= hero2Cost * 2;
            coinsValue = Math.Round(coinsValue, 2);
            coinsText.SetText(coinsValue.ToString());
            Hero2Multiplier += baseMultiplier * 2;
            hero2Cost *= heroCostMultiplier * 2;
        }
        else
            return;
    }

    void Hero1CoinsMultiplier()
    {
        coinsValue += Hero1Multiplier;
        coinsValue = Math.Round(coinsValue, 2);
        coinsText.SetText(coinsValue.ToString());
    }

    void Hero2CoinsMultiplier()
    {
        coinsValue += Hero2Multiplier;
        coinsValue = Math.Round(coinsValue, 2);
        coinsText.SetText(coinsValue.ToString());
    }

    public void ClickingIncrements()
    {
        coinsValue += clickMultiplier;
        coinsText.SetText(coinsValue.ToString());
    }
}
