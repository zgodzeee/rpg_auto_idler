using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneTransition : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SceneSettings()
    {
        SceneManager.LoadScene("SettingsMenu");
    }

    public void SceneMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }

    public void ScenePlay()
    {
        SceneManager.LoadScene("PlayScene");
    }
}
